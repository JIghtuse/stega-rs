RUSTC := rustc
MKDIR := mkdir -p
BIN := bin/detect

all: $(BIN)

$(BIN): src/main.rs src/bmp.rs src/rs.rs
	$(MKDIR) bin
	$(RUSTC) $< -o $@

run: $(BIN)
	./bin/detect ../content/1.bmp

clean:
	rm -f $(BIN)
