use bmp::BitmapContainer;
use std::iter::range_step;

struct GroupStat {
    nregular: uint,
    nsingular: uint,
}

fn u_abs_sub(a: u8, b: u8) -> u8 {
    if a > b {
        return a - b;
    } else {
        return b - a;
    }
}

fn abs_min(a: f64, b: f64) -> f64 {
    if a.abs() < b.abs() {
        return a;
    } else {
        return b;
    }
}

fn calc_group_smoothness(values: Vec<u8>) -> uint {
    let mut smoothness = 0u;
    for i in range(0, values.len() - 1) {
        let diff = u_abs_sub(*values.get(i + 1), *values.get(i));
        smoothness += diff as uint;
    }
    return smoothness;
}

fn fplus1(val: u8) -> u8 {
    return val ^ 1;
}

fn fminus1(val: u8) -> u8 {
    return fplus1(val + 1) - 1;
}

fn flip_vector(v: Vec<u8>, mask: Vec<int>) -> Vec<u8> {
    let mut vres = v;
    for i in range(0, mask.len()) {
        let val = *vres.get(i);
        match *mask.get(i) {
            1 => vres.grow_set(i, &val, fplus1(val)),
            -1 => vres.grow_set(i, &val, fminus1(val)),
            0 => (),
            _ => fail!("Incorrect mask value! {:}", mask),
        }
    }

    return vres;
}

fn invert_mask(mask: Vec<int>) -> Vec<int> {
    let mut mres = mask;
    for i in mres.mut_iter() {
        *i *= -1;
    }
    return mres;
}

fn calc_groupstat(container: &BitmapContainer, mask: Vec<int>, color: uint) -> GroupStat {
    let mut nreg = 0;
    let mut nsing = 0;

    let ncolors = container.ncolors;
    let mask_len = mask.clone().len();
    let step = mask_len * ncolors;

    for j in range_step(0, container.size - mask_len + 1, step) {
        let mut v = vec!();
        for k in range_step(0, ncolors * mask_len, ncolors) {
            let idx = j + k + color;
            v.push(container.getbyte(idx));
        }

        let smoothness = calc_group_smoothness(v.clone());
        let flip_smoothness = calc_group_smoothness(flip_vector(v, mask.clone()));
        if smoothness < flip_smoothness {
            nreg += 1;
        } else if smoothness > flip_smoothness {
            nsing += 1;
        }
    }
 
    return GroupStat {
        nregular: nreg,
        nsingular: nsing,
    };
}

fn calc_smaller_root(a: f64, b: f64, c: f64) -> f64 {
    let discr = b * b - 4.0 * a * c;

    if discr < 0.0 {
        return 0.0;
    }

    return match a {
        0.0 => -c as f64 / b as f64,
        _ => {
            let root1 = ((-b) as f64 + (discr as f64).sqrt()) / (2.0 * a as f64);
            let root2 = ((-b) as f64 - (discr as f64).sqrt()) / (2.0 * a as f64);
            abs_min(root1, root2)
        }
    };
}

pub fn get_msglen(mut container: BitmapContainer) -> f64 {
    let mask = vec!(0, 1, 1, 0);
    let mut s = 0.0;

    for color in range(0, container.ncolors) {

        let gstat = calc_groupstat(&container, mask.clone(), color);

        let invmask = invert_mask(mask.clone());
        let gstat_inv = calc_groupstat(&container, invmask.clone(), color);

        container.flip_lsbs();

        let gstat_flip = calc_groupstat(&container, mask.clone(), color);
        let gstat_flip_inv = calc_groupstat(&container, invmask.clone(), color);
        
        container.flip_lsbs();

        let d0 : int = gstat.nregular as int - gstat.nsingular as int;
        let dmin0 : int = gstat_inv.nregular as int - gstat_inv.nsingular as int;
        let d1 : int = gstat_flip.nregular as int - gstat_flip.nsingular as int;
        let dmin1 : int = gstat_flip_inv.nregular as int - gstat_flip_inv.nsingular as int;

        let a = 2 * (d1 + d0);
        let b = dmin0 - dmin1 - d1 - 3 * d0;
        let c = d0 - dmin0;

        let x = calc_smaller_root(a as f64, b as f64, c as f64);
        s += x / (x - 0.5);
    }
    return s / container.ncolors as f64;
}
