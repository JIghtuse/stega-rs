use bmp::{load_image};
mod bmp;
mod rs;

fn main() {
    let args = std::os::args();
    if args.len() < 2 {
        fail!("Usage: {:s} <filename.bmp>", args.get(0).as_slice());
    }

    let fname = args.get(1).as_slice();
    let container = bmp::load_image(fname);

    let msglen = rs::get_msglen(container);

    println!("{:f}", msglen);
}
