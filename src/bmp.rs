use std::io::File;
use std::vec::Vec;

struct BitmapFileHeader {
    header: Vec<char>,
    fsize: u32,
    res1: u16,
    res2: u16,
    data_offset: uint
}

#[inline]

pub struct BitmapContainer {
    header: Vec<u8>,
    pub size: uint,
    bytes: Vec<u8>,
    pub ncolors: uint,
}

impl Clone for BitmapContainer {
    fn clone(&self) -> BitmapContainer {
        return BitmapContainer {
            header: self.header.clone(),
            size: self.size,
            bytes: self.bytes.clone(),
            ncolors: self.ncolors,
        }
    }
}

impl BitmapContainer {
    pub fn getbyte(&self, pos: uint) -> u8 {
        return *self.bytes.get(pos);
    }
    pub fn putbyte(&mut self, pos: uint, b: u8) {
        self.bytes.grow_set(pos, &b, b);
    }
    pub fn flip_lsbs(&mut self) {
        for byte in self.bytes.mut_iter() {
            *byte ^= 1;
        }
    }
}

fn u8_pack_u32(arr: &[u8]) -> u32 {
    return arr[0] as u32
         | arr[1] as u32 << 8
         | arr[2] as u32 << 16
         | arr[3] as u32 << 24;
}
#[inline]
fn u8_pack_u16(arr: &[u8]) -> u16 {
    return arr[0] as u16 | arr[1] as u16 << 8;
}

fn get_width(contents: &Vec<u8>) -> uint {
    let dib_header_sz = u8_pack_u32(contents.slice(14, 18));

    return match dib_header_sz {
        12 => u8_pack_u16(contents.slice(18, 20)) as uint,
        _ => u8_pack_u32(contents.slice(18, 22)) as uint,
    };
}

fn get_height(contents: &Vec<u8>) -> uint {
    let dib_header_sz = u8_pack_u32(contents.slice(14, 18));

    return match dib_header_sz {
        12 => u8_pack_u16(contents.slice(20, 22)) as uint,
        _ => u8_pack_u32(contents.slice(22, 26)) as uint,
    };
}

fn get_bpp(contents: &Vec<u8>) -> uint {
    let dib_header_sz = u8_pack_u32(contents.slice(14, 18));

    return match dib_header_sz {
        12 => u8_pack_u16(contents.slice(24, 26)) as uint,
        _ => u8_pack_u16(contents.slice(28, 30)) as uint,
    };
}

pub fn load_image(fname: &str) -> BitmapContainer {
    let path = Path::new(fname);
    let mut file = match File::open(&path) {
        Ok(f) => f,
        Err(e) => fail!("file opening error: {}", e),
    };

    let contents = file.read_to_end().unwrap();
    let fheader = BitmapFileHeader {
        header: vec!(*contents.get(0) as char, *contents.get(1) as char),
        fsize: u8_pack_u32(contents.slice(2,6)),
        res1: u8_pack_u16(contents.slice(6,8)),
        res2: u8_pack_u16(contents.slice(8, 10)),
        data_offset: u8_pack_u32(contents.slice(10, 14)) as uint,
    };
    // FIXME: we need to check header
 
    let bpp = get_bpp(&contents);
    if bpp != 24 {
        fail!("Cannot process image with bpp != 24");
    }

    let ncolors = bpp / 8;

    let width = get_width(&contents);
    let height = get_height(&contents);
    let nbytes = width * height * ncolors;

    let bytes = contents.slice(fheader.data_offset, fheader.data_offset+nbytes);
    let header = contents.slice(0, fheader.data_offset);

    // FIXME: need to find ncolors
    return BitmapContainer {
        header: Vec::from_slice(header),
        size: nbytes,
        bytes: Vec::from_slice(bytes),
        ncolors: ncolors,
    };
}

pub fn save_image(old_path_str: &str, prefix: &str, container: BitmapContainer) -> bool {
    let old_path = Path::new(old_path_str);
    let old_fname = old_path.filename_str().unwrap();
 
    // 1.bmp => inj_rrand_5.9999_1.bmp
    let mut new_fname = prefix.to_string();
    new_fname = new_fname.append(old_fname);

    let new_path_str = old_path.dirname_str().unwrap().to_string();

    let mut new_path = Path::new(new_path_str);
    new_path.push(new_fname);

    let mut file = File::create(&new_path);

    file.write(container.header.as_slice()).unwrap();
    file.write(container.bytes.as_slice()).unwrap();
    return true;

}
