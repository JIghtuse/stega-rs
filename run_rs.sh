#!/bin/sh

FNAME_LOG_CLEAN="results_clean.txt"
FNAME_PREFIX="results_"
CONTENT_PATH="../content/"

INJECT_BIN=$HOME/code/master/steganography/inject/bin/inject

run_clean() {
  for i in $(seq 1 1000); do
      ./bin/detect ../content/$i.bmp | tee -a $FNAME_LOG_CLEAN
  done
}

run_inject() {
for percentage in 0.25 5 30; do
  echo "percentage: $percentage"
  for i in $(seq 1 1000); do

    $INJECT_BIN rrnd $CONTENT_PATH$i.bmp $percentage > /dev/null
    FNAME=$CONTENT_PATH/inj_rrand_*_$i.bmp
    echo "processing file $FNAME"
    ./bin/detect $FNAME >> $FNAME_PREFIX$percentage.txt

    rm $FNAME
  done
done
}

run_clean
