#!/usr/bin/gnuplot -p

set term pngcairo enhanced size 800,600
set grid
set key bottom right reverse Right box 1
set output 'rs.png'
plot [0:1] 'roc.data' u 1:2 title '2.5%' w lines, \
           'roc.data' u 1:3 title '5%' w lines, \
           'roc.data' u 1:4 title '30%' w lines, \
           x w lines
